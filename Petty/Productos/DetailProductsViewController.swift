//
//  DetailProductsViewController.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

class DetailProductsViewController: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    let alertService = AlertService()
    @IBOutlet weak var nameView: UILabel!
    
    @IBOutlet weak var veterinariaView: UILabel!
    
    @IBOutlet weak var precioView: UILabel!
    
    @IBOutlet weak var carrito: UIBarButtonItem!
    var image = UIImage()
    var name = ""
    var veterinaria = ""
    var precio = ""
    var urlProductos = ""
    var tipo = ""
    var imFileName = ""
    var usuario = ""

    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        imageView.image = image
        nameView.text = "Descripción: "+name
        veterinariaView.text = "Veterinaria: "+veterinaria
        precioView.text = "Precio: "+precio
        
        imageView.kf.indicatorType = .activity
        
        imageView.kf.setImage(with: URL(string: self.urlProductos), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func carrito(_ sender: Any)
    {
        performSegue(withIdentifier: "shoppingList", sender: self)
    }
    
    @IBAction func onClick(_ sender: Any) {
        
        //
        
        let mascotasReference = Firestore.firestore().collection("usuarios")
        
        
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            guard let snapshot = snapshot else { return }
            
            if !snapshot.documents.isEmpty
            {
                for document in snapshot.documents
                {
                    self.usuario = document.data()["userId"] as! String
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            
        self.carrito.isEnabled = true
        
        let carritoReference = Firestore.firestore().collection("carrito")
        let parameters: [String: Any] = ["name":self.name,"precio":self.precio,"tipo":self.tipo,"url":self.urlProductos,"veterinaria":self.veterinaria,"imageFileName":self.imFileName,"userId":self.usuario]
        
        carritoReference.addDocument(data: parameters)
        
        let alert = UIAlertController(title: "Confirmación", message: "Este producto se ha añadido correctamente a tu carrito de compras. Para solicitar el pedido selecciona el carrito y luego oprime solicitar.", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(subButton)
        self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var vc = segue.destination as! ShoppingListViewController
    }

}
