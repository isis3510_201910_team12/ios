//
//  CollectionViewController.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 3/23/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//
//ios

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

struct Productos {
    var nombre:String
    var precio:String
    var url:String
    var veterinaria:String
}

class CollectionViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var productosArray = [Productos]()
    var titlesF = [String]()
    var vetF = [String]()
    var pricF = [String]()
    var imagesF = [UIImage]()
    var urlProductos = [String]()
    var nombresImagenes = [String]()
    var tipo: String!
    
    let prueba = [UIImage(named: "productsTab"),
                  UIImage(named: "productsTab"),
                  UIImage(named: "productsTab"),
                  UIImage(named: "productsTab")]
    
    let relativeFontWelcomeTitle:CGFloat = 0.045
    let relativeFontButton:CGFloat = 0.060
    let relativeFontCellTitle:CGFloat = 0.023
    let relativeFontCellDescription:CGFloat = 0.015
    
    var imageReference: StorageReference{
        return Storage.storage().reference().child("Resources")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.tipo == "alimento"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "medicamentos"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "juguetes"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "casas"
        {
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "collares"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "snacks"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        else if self.tipo == "accesorios"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
        else if self.tipo == "otros"
        {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func readSnacks()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                print(document.data())
                let data = document.data()
                if document.data()["tipo"] as!String == "snacks"
                {
                    
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readAccesorios()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "accesorios"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readOtros()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "otros"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readJuguetes()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "juguetes"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readCollares()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "collares"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readCasas()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "casas"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readMedicamentos()
    {
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "medicamento"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readProductos()
    {
        
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                
                let data = document.data()
                if document.data()["tipo"] as!String == "alimento"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
                
            }
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesF.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cellIndex = indexPath.item
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailProductsViewController") as? DetailProductsViewController
        
        vc?.name = self.titlesF[cellIndex]
        vc?.urlProductos = self.urlProductos[cellIndex]
        vc?.veterinaria = self.vetF[cellIndex]
        vc?.precio = self.pricF[cellIndex]
        vc?.tipo = self.tipo
        vc?.imFileName = self.nombresImagenes[cellIndex]
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
            let cellIndex = indexPath.item
            if (self.urlProductos.isEmpty)
            {
                let alert = UIAlertController(title: "No tienes internet ", message: "Requieres de una conexion de internet para poder ver la imagen de los productos  ", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
                
                cell.imageView.image = self.prueba[cellIndex]
                
            }
            else
            {
                cell.imageView.kf.indicatorType = .activity
                
                cell.imageView.kf.setImage(with: URL(string: self.urlProductos[cellIndex]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        
            cell.nameView.text = "Descripción: "+self.titlesF[cellIndex]
            
            cell.nameView.numberOfLines = 0
            cell.contentView.layer.cornerRadius = 10
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.white.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.backgroundColor = UIColor.white
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

        return cell
    }
    
    
    
    @IBAction func carrito(_ sender: Any)
    {
        performSegue(withIdentifier: "carrito", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var vc = segue.destination as! UIViewController
    }
    
}
