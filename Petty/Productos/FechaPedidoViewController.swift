//
//  FechaPedidoViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class FechaPedidoViewController: UIViewController {
    
    var titlesF = [String]()
    var tiposF = [String]()
    var vetF = [String]()
    var pricF = [String]()
    var urlProductos = [String]()
    var nombresImagenes = [String]()
    var lat = ""
    var long = ""

    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func solicitarPedido(_ sender: Any)
    {
        
        performSegue(withIdentifier: "pedido2", sender: self)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! ProcessingPedidoViewController
        vc.titlesF = self.titlesF
        vc.vetF = self.vetF
        vc.pricF = self.pricF
        vc.urlProductos = self.urlProductos
        vc.nombresImagenes = self.nombresImagenes
        vc.lat = self.lat
        vc.long = self.long
        
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yy, H:mm"
        
        vc.fecha = formater.string(from:datePicker.date)
        
        vc.tiposF = self.tiposF
    }
}
