//
//  ShoppingListCollectionViewCell.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 4/25/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class ShoppingListCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var nameView: UILabel!
    
    @IBOutlet weak var veterinariaView: UILabel!
    
    @IBOutlet weak var precioView: UILabel!
    
}
