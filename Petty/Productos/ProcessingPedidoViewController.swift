//
//  ProcessingPedidoViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class ProcessingPedidoViewController: UIViewController {
    
    var titlesF = [String]()
    var tiposF = [String]()
    var vetF = [String]()
    var pricF = [String]()
    var urlProductos = [String]()
    var nombresImagenes = [String]()
    var lat = ""
    var long = ""
    var fecha = ""

    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        let serviciosReference = Firestore.firestore().collection("pedidos")
        var i = 0
        
        for element in titlesF
        {
            
            let parameters: [String: Any] = ["name":self.titlesF[i], "precio":self.pricF[i], "longitud": self.long, "latitud":self.lat,"tipo":self.tiposF[i], "url":self.urlProductos[i], "veterinaria":self.vetF[i], "imageFileName":self.nombresImagenes[i],"fecha":self.fecha]
            
            serviciosReference.addDocument(data: parameters)
            i = i + 1
        }
        
        let alert = UIAlertController(title: "Confirmación", message: "Tu pedido ha sido registrado exitosamente.", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "Ok", style: .default, handler: self.regresar)
        
        alert.addAction(subButton)
        self.present(alert, animated: true, completion: nil)
        
        // Do any additional setup after loading the view.
    }
    
    func regresar(alert: UIAlertAction)
    {
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let x = storyBoard.instantiateViewController(withIdentifier: "y") as! CustomTabBarViewController
        self.present(x, animated: true, completion: nil)
    }
}
