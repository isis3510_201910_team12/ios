
//
//  MapaPedidoViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import CoreLocation

class MapaPedidoViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    var titlesF = [String]()
    var tiposF = [String]()
    var vetF = [String]()
    var pricF = [String]()
    var urlProductos = [String]()
    var nombresImagenes = [String]()
    var lati = ""
    var longi = ""
    
    var cammara:GMSCameraPosition!
    var googleMapsView: GMSMapView!
    let locationManager = CLLocationManager()
    
    @IBOutlet var googleMapsViewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.showCurrentLocationOnMap()
        self.locationManager.stopUpdatingLocation()
    }
    
    @IBAction func continuar(_ sender: Any)
    {
        performSegue(withIdentifier: "pedido1", sender: self)
    }
    
    func showCurrentLocationOnMap()
    {
        lati = "\(self.locationManager.location?.coordinate.latitude ?? 0.0)"
        longi = "\(self.locationManager.location?.coordinate.longitude ?? 0.0)"
        
        cammara = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 17.0)
        
        googleMapsView = GMSMapView.map(withFrame: self.googleMapsViewContainer.frame, camera: cammara)
        googleMapsView.delegate = self
        //view = mapView
        
        googleMapsView.settings.myLocationButton = true
        googleMapsView.isMyLocationEnabled = true
        
        let markker = GMSMarker()
        markker.position = cammara.target
        markker.snippet = "Current Location"
        markker.map = googleMapsView
        self.view.addSubview(self.googleMapsView)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        
        let cammaraa = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17.0)
        
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: cammaraa)
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        let markker = GMSMarker()
        markker.position = cammaraa.target
        markker.snippet = "Current Location"
        markker.map = mapView
        self.view.addSubview(mapView)
        view = mapView
        
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String)
    {
        
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position: position)
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
            self.googleMapsView.camera = camera
            
            marker.title = "Address : \(title)"
            marker.map = self.googleMapsView
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! FechaPedidoViewController
        vc.titlesF = self.titlesF
        vc.vetF = self.vetF
        vc.pricF = self.pricF
        vc.urlProductos = self.urlProductos
        vc.nombresImagenes = self.nombresImagenes
        vc.lat = self.lati
        vc.long = self.lati
        vc.tiposF = self.tiposF
    }

}
