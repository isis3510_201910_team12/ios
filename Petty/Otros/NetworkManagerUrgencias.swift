//
//  NetworkManagerUrgencias.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/9/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Reachability

class NetworkManagerUrgencias: NSObject{
    var reachability: Reachability!
    
    static let sharedInstance: NetworkManagerUrgencias = {
        return NetworkManagerUrgencias()
    }()
    
    override init() {
        super.init()
        
        // Initialise reachability
        reachability = Reachability()!
        
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
    }
    
    
    static func stopNotifier() -> Void {
        NetworkManagerUrgencias.sharedInstance.reachability.stopNotifier()
        
        do {
            // Stop the network status notifier
            try (NetworkManagerUrgencias.sharedInstance.reachability).stopNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    
    static func startNotifier() -> Void {
        do {
           
            try (NetworkManagerUrgencias.sharedInstance.reachability).startNotifier()
        } catch {
            print("Error starting notifier")
        }
    }
}
