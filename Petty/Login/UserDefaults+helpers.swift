//
//  UserDefaults+helpers.swift
//  Petty
//
//  Created by Camilo on 6/11/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import Foundation

extension UserDefaults{
    
    func setIsLoggedIn(value: Bool)
    {
        set(value, forKey: "isLoggedIn")
        synchronize()
    }
    
    func setNumber(value: String)
    {
        set(value, forKey: "phoneNumber")
        synchronize()
    }
    
    func isLoggedIn() -> Bool
    {
        return bool(forKey: "isLoggedIn")
    }
    
    func getNumber() -> String
    {
        return string(forKey: "phoneNumber")!
    }
}
