//
//  OrigenViewController.swift
//  Petty
//
//  Created by Camilo on 6/11/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class OrigenViewController: UIViewController {
    
    var tipo: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if UserDefaults.standard.isLoggedIn()
        {
            self.tipo = "siga"
            self.performSegue(withIdentifier: "siga", sender: self)
        }
        else
        {
            self.tipo = "logearse"
            self.performSegue(withIdentifier: "logearse", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if self.tipo == "siga"
        {
            var vc = segue.destination as! CustomTabBarViewController
        }
        else
        {
            var vc = segue.destination as! StartViewController
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
