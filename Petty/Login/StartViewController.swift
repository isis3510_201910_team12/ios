//
//  StartViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase

class StartViewController: UIViewController,  UITextFieldDelegate {
    
    var tipo: String!
    var cumple: String!

    @IBOutlet weak var phoneNum: UITextField!
    
    @IBOutlet weak var codigoTxt: UITextField!
    @IBOutlet weak var registrarmeBtn: UIButton!
    @IBOutlet weak var enviarBtn: UIButton!
    
    var verificationId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phoneNum.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.tipo == "logout"
        {
            UserDefaults.standard.setIsLoggedIn(value: false)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        view.resignFirstResponder()
        return true
    }
    
    @IBAction func registrarse(_ sender: Any) {
        
        guard let verificationCode = codigoTxt.text else {return}
        guard let phoneNumber = phoneNum.text else {return}
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationId,
            verificationCode: verificationCode)
        
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                print(error)
                return
            }
            // User is signed in
            // ...
            
            UserDefaults.standard.setIsLoggedIn(value: true)
            UserDefaults.standard.setNumber(value: phoneNumber)
            
            let usuariosReference = Firestore.firestore().collection("usuarios")
            let parameters: [String: Any] = ["num":phoneNumber]
            usuariosReference.addDocument(data: parameters)
            
            
            self.performSegue(withIdentifier: "strt1", sender: self)
        }
    }
    
    @IBAction func ingresar(_ sender: Any) {
        
        guard let phoneNumber = phoneNum.text else {return}
        
        Auth.auth().languageCode = "es"
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            // Change language code to french.
            
            self.verificationId = verificationID
            self.codigoTxt.isHidden = false
            self.registrarmeBtn.isHidden = false
            self.enviarBtn.isHidden = true
        
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       
            var vc = segue.destination as! CustomTabBarViewController
        
        
        
        
    }

}
