//
//  AdoptarViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase

import FirebaseFirestore

import FirebaseStorage

import Kingfisher


class AdoptarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var tipo: String!

    var titlesF = [String]()
    
    var racesF = [String]()
    
    var agesF = [String]()
    
    var vetsF = [String]()
    
    var imagesF = [UIImage]()
    
    var urlAdoptadas = [String]()
    
    var nombresImagenes = [String]()
    
    var ids = [String]()
    
    let prueba = [UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab")]
    
    let network: NetworkManager = NetworkManager.sharedInstance
    
    let alertService = AlertService()
    
    
    let relativeFontWelcomeTitle:CGFloat = 0.045
    
    let relativeFontButton:CGFloat = 0.060
    
    let relativeFontCellTitle:CGFloat = 0.023
    
    let relativeFontCellDescription:CGFloat = 0.015
    
    
    var imageReference: StorageReference{
        
        return Storage.storage().reference().child("Resources")
        
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            
            self.collectionView.reloadData()
            
        }
        
        collectionView.delegate = self
        
        collectionView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
    }
    
    func showOfflinePage()
    {
        let alertVC = alertService.alert(title: "No hay conexion a internet ", body: "Es posible que no puedas ver las últimas actualizaciones, para poder hacerlo conectate a internet. ", buttonTitle: "Entendido") {
            
        }
        
        present(alertVC, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func read()
    {
        let productsReference = Firestore.firestore().collection("adoptadas")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                print(document.data())
                let data = document.data()
                
                let name = document.data()["name"] ?? ""
                let edad = document.data()["age"] ?? ""
                let raza = document.data()["race"] ?? ""
                let veterinaria = document.data()["veterinaria"] ?? ""
                let ifn = document.data()["imageFileName"] ?? ""
                self.nombresImagenes.append(ifn as! String)
                self.urlAdoptadas.append(data["urlImage"] as! String)
                self.titlesF.append(name as! String)
                self.racesF.append(raza as! String)
                self.agesF.append(edad as! String )
                self.vetsF.append(veterinaria as! String)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesF.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cellIndex = indexPath.item
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailAdoptadasViewController") as? DetailAdoptadasViewController
        
        vc?.name = self.titlesF[cellIndex]
        vc?.urlAdoptadas = self.urlAdoptadas[cellIndex]
        vc?.edad = self.agesF[cellIndex]
        vc?.raza = self.racesF[cellIndex]
        vc?.veterinaria = self.vetsF[cellIndex]
        vc?.imFileName = self.nombresImagenes[cellIndex]
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AdoptarCollectionViewCell
        
            let cellIndex = indexPath.item
            
            if (self.urlAdoptadas.isEmpty)
                
            {
                
                let alert = UIAlertController(title: "No tienes internet ", message: "Requieres de una conexion de internet para poder ver la imagen de las mascotas  ", preferredStyle: .alert)
                
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                
                self.present(alert, animated: true, completion: nil)
                
                cell.imageView.image = self.prueba[cellIndex]
                
            }
                
            else
                
            {
                
                cell.imageView.kf.indicatorType = .activity

                print(cellIndex)
                    cell.imageView.kf.setImage(with: URL(string: self.urlAdoptadas[cellIndex]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                    cell.nameView.text = "Descripción: "+self.titlesF[cellIndex]
                    cell.raceView.text = self.racesF[cellIndex]
                    cell.ageView.text = self.agesF[cellIndex]
                    cell.veterinariaView.text = self.vetsF[cellIndex]
            }
        
            cell.contentView.layer.cornerRadius = 10
            
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.white.cgColor
            
            cell.contentView.layer.masksToBounds = true
            
            cell.backgroundColor = UIColor.white
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            
            cell.layer.shadowRadius = 2.0
            
            cell.layer.shadowOpacity = 1.0
            
            cell.layer.masksToBounds = false
            
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

        return cell
        
    }
    

}
