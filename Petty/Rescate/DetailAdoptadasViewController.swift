//
//  DetailAdoptadasViewController.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class DetailAdoptadasViewController: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    
    @IBOutlet weak var nameView: UILabel!
    
    
    @IBOutlet weak var raceView: UILabel!
    
    
    @IBOutlet weak var ageView: UILabel!
    
    
    @IBOutlet weak var veterinariaView: UILabel!
    
    var image = UIImage()
    var name = ""
    var edad = ""
    var raza = ""
    var urlAdoptadas = ""
    var imFileName = ""
    var veterinaria = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.viewDidLoad()
        imageView.image = image
        nameView.text = name
        ageView.text = edad
        raceView.text = raza
        veterinariaView.text = veterinaria
        
        imageView.kf.indicatorType = .activity
        
        imageView.kf.setImage(with: URL(string: self.urlAdoptadas), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func enviarSolicitud(_ sender: Any) {
        
        performSegue(withIdentifier: "adoptPetForm", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! AdoptFormViewController
        vc.image = image
        vc.urlProductos = urlAdoptadas
    }

}
