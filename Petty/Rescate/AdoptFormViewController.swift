//
//  AdoptFormViewController.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

class AdoptFormViewController: UIViewController {
    
    
    let alertService = AlertService()
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var nameView: UITextField!
    
    
    @IBOutlet weak var emailView: UITextField!
    
    
    @IBOutlet weak var descriptionView: UITextView!
    
    @IBOutlet weak var lblValidationMessage: UILabel!
    
    var image = UIImage()
    
    var urlProductos = ""
    var empty = false
    
    var nombre = ""
    var correo = ""
    var descripcion = ""
    var url = ""
    
    var imageReference: StorageReference{
        return Storage.storage().reference() .child("Resources")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblValidationMessage.isHidden = true
        self.descriptionView.layer.cornerRadius = 8
        self.descriptionView.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).cgColor
        self.descriptionView.layer.borderWidth = 1.0
        
        
        self.nameView.layer.cornerRadius = 8
        self.nameView.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).cgColor
        self.nameView.layer.borderWidth = 1.0
        
        
        self.emailView.layer.cornerRadius = 8
        self.emailView.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).cgColor
        self.emailView.layer.borderWidth = 1.0
        
        
        imageView.image = image
        
        imageView.kf.indicatorType = .activity
        
        imageView.kf.setImage(with: URL(string: self.urlProductos), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func notEmpty() -> Bool {
        
        
        if (nameView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "No puedes enviar sin un nombre"
            empty = true
            return empty
        }
        if (emailView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "No puedes enviar sin un correo"
            empty = true
            return empty
        }
        if (descriptionView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "No puedes enviar sin una descripción"
            empty = true
            return empty
        }
        return false
        
    }
    
    func textField(_ textView: UITextView) {
        if(nameView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "Recuerda que debes introducir un nombre "
        }
        if(emailView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "Recuerda que debes introducir un correo "
        }
        if(descriptionView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "Recuerda que debes introducir una descripción "
        }
        else
        {
            lblValidationMessage.isHidden = true
        }
    }
    
    

    @IBAction func tapEnviar(_ sender: Any) {
        
        if notEmpty() == false
        {
            self.enviar()
            let alert = UIAlertController(title: "Confirmación", message: "Tu solicitud ha sido enviada correctamente a la veterinaria.Se te enviara un correo con un formulario que debes diligenciar", preferredStyle: .alert)
            let subButton = UIAlertAction(title: "Ok", style: .default, handler: self.regresar)
            
            alert.addAction(subButton)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func enviar()
    {
        
        self.nombre = self.nameView.text!
        self.correo = self.emailView.text!
        self.descripcion = self.descriptionView.text
        self.url = self.urlProductos
        
        let serviciosReference = Firestore.firestore().collection("solicitudesAdoptadas")
        let parameters: [String: Any] = ["name": nombre, "email": correo, "description": descripcion,"url": url]
        
        serviciosReference.addDocument(data: parameters)
    }
    
    func regresar(alert: UIAlertAction)
    {
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let x = storyBoard.instantiateViewController(withIdentifier: "y") as! CustomTabBarViewController
        self.present(x, animated: true, completion: nil)
    }
    
}
