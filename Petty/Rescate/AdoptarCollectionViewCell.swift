//
//  AdoptarCollectionViewCell.swift
//  Petty
//
//  Created by JHON JAIRO GONZALEZ MELO on 4/26/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class AdoptarCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var nameView: UILabel!
    
    
    
    @IBOutlet weak var raceView: UILabel!
    
    
    @IBOutlet weak var ageView: UILabel!
    
    
    @IBOutlet weak var veterinariaView: UILabel!
    
}
