//

//  ListaRescateViewController.swift

//  Petty

//

//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 3/26/19.

//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.

//


import UIKit


import Firebase

import FirebaseFirestore

import FirebaseStorage

import Kingfisher


class ListaRescateViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tipo: String!
    
    
    
    
    
    var titlesF = [String]()
    
    var imagesF = [UIImage]()
    
    var urlEncontradasPerdidas = [String]()
    
    let prueba = [UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab"),
                  
                  UIImage(named: "RescueTab")]
    
    let network: NetworkManager = NetworkManager.sharedInstance
    
    let alertService = AlertService()
    
    
    
    let relativeFontWelcomeTitle:CGFloat = 0.045
    
    let relativeFontButton:CGFloat = 0.060
    
    let relativeFontCellTitle:CGFloat = 0.023
    
    let relativeFontCellDescription:CGFloat = 0.015
    
    
    
    var imageReference: StorageReference{
        
        return Storage.storage().reference().child("Resources")
        
    }
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if self.tipo == "encontradas"
            
        {
            
            
            
            DispatchQueue.main.async {
                
                self.collectionView.reloadData()
                
            }
            
            
            
            collectionView.delegate = self
            
            collectionView.dataSource = self
            
            
            
        }
            
        else
            
        {
            
            DispatchQueue.main.async {
                
                self.collectionView.reloadData()
                
            }
            
            
            
            collectionView.delegate = self
            
            collectionView.dataSource = self
            
            
            
            
            
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
    }
    
    func showOfflinePage()
    {
        let alertVC = alertService.alert(title: "No hay conexion a internet ", body: "Es posible que no puedas ver las últimas actualizaciones, para poder hacerlo conectate a internet. ", buttonTitle: "Entendido") {
            
        }
        
        present(alertVC, animated: true)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    
    
    func readEncontradas()
        
    {
        
        let mascotasReference = Firestore.firestore().collection("encontradas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            
            
            guard let snapshot = snapshot else { return }
            
            
            
            var c = 1;
            
            for document in snapshot.documents
                
            {
                
                let data = document.data()
                
                
                self.titlesF.append(data["descripcion"] as! String)
                
                
                let ifn = document.data()["imageFileName"] ?? ""
                
                self.urlEncontradasPerdidas.append(data["url"] as! String)
                
            }
            
        }
        
    }
    
    
    
    func readPerdidas()
        
    {
        
        let mascotasReference = Firestore.firestore().collection("perdidas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            
            
            guard let snapshot = snapshot else { return }
            
            
            
            var c = 1;
            
            for document in snapshot.documents
                
            {
                
                let data = document.data()
                
                self.titlesF.append(data["descripcion"] as! String)
                
                let ifn = document.data()["imageFileName"] ?? ""
                
                self.urlEncontradasPerdidas.append(data["url"] as! String)
                
            }
            
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.titlesF.count
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
        
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! PerdidasEncontradasCollectionViewCell
            
            let cellIndex = indexPath.item

            if (self.urlEncontradasPerdidas.isEmpty)
                
            {
                
                let alert = UIAlertController(title: "No tienes internet ", message: "Requieres de una conexion de internet para poder ver la imagen de las mascotas  ", preferredStyle: .alert)
                
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                
                
                alert.addAction(subButton)
                
                self.present(alert, animated: true, completion: nil)
                
                
                
                cell.imageView.image = self.prueba[cellIndex]
                
            }
                
            else
                
            {
                
                cell.imageView.kf.indicatorType = .activity
                
                
                
                cell.imageView.kf.setImage(with: URL(string: self.urlEncontradasPerdidas[cellIndex]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                
            }
            
            cell.nameView.text = "Descripción: "+self.titlesF[cellIndex]
            
            cell.contentView.layer.cornerRadius = 10
            
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.white.cgColor
            
            cell.contentView.layer.masksToBounds = true
            
            cell.backgroundColor = UIColor.white
            
            
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            
            cell.layer.shadowRadius = 2.0
            
            cell.layer.shadowOpacity = 1.0
            
            cell.layer.masksToBounds = false
            
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

        return cell
        
    }
    
    
}

