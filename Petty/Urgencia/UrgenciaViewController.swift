//
//  UrgenciaViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/22/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseStorage
import Reachability


class UrgenciaViewController: UIViewController, UITextViewDelegate, AVAudioRecorderDelegate , AVAudioPlayerDelegate , UINavigationControllerDelegate , UIImagePickerControllerDelegate  {
    
    
    @IBOutlet weak var lblValidationMessage: UILabel!
    @IBOutlet weak var lblValidationRecord: UILabel!
    @IBOutlet weak var myTextView: UITextView!
    var textInput = ""
    var usuario = ""
    
    @IBOutlet weak var imageButton: UIButton!
    var reachability: Reachability!
    
    @IBOutlet weak var recordButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    
    
    var soundRecorder : AVAudioRecorder!
    
    var soundPlayer : AVAudioPlayer!
    
    
    var fileName : String =  "audioFile.m4a"
    
    
    var recordingSession : AVAudioSession!
    
    var audioRecorder : AVAudioRecorder!
    
    
    var imageReference: StorageReference{
        return Storage.storage().reference() .child("images")
    }
    
    
    var imageFfileName: String = "image.jpg"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reachability = Reachability()!
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        // record audio
        
        
        // finish record audio
        //
        
        self.myTextView.layer.cornerRadius = 8
        self.myTextView.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).cgColor
        self.myTextView.layer.borderWidth = 1.0
        
        myTextView.delegate = self
        
        lblValidationMessage.isHidden = false
        lblValidationMessage.text = "Añade una descripción o graba una nota de voz"
        
        lblValidationRecord.isHidden = true
        setupRecorder()
        playButton.isEnabled = false
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        
        let reachibility = notification.object as! Reachability
        switch reachibility.connection{
        case .none:
            showOfflinePage()
        case .wifi:
            print()
        case .cellular:
            print()
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if(myTextView.text == "")
        {
            lblValidationMessage.isHidden = false
            lblValidationMessage.text = "Añade una descripción o graba una nota de voz"
        }
        else
        {
            lblValidationMessage.isHidden = true
        }
    }
    
    func showOfflinePage() {
        
        let alert = UIAlertController(title: "No hay conexión a internet", message: "Sin conexión a internet no puedes solicitar una urgencia veterinaria, sin embargo puedes llamar a las veterinarias cercanas y solicitar ayuda.", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "Ver veterinarias", style: .default, handler: solicitarSinInternet)
        let subButton2 = UIAlertAction(title: "Regresar", style: .cancel, handler: regresar)
        
        alert.addAction(subButton)
        alert.addAction(subButton2)
        self.present(alert, animated: true, completion: nil)
    }
    
    func solicitarSinInternet(alert: UIAlertAction)
    {
        performSegue(withIdentifier: "noConex", sender: self)
    }
    
    func regresar(alert: UIAlertAction)
    {
        self.navigationController!.popToRootViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    
    @IBAction func send(_ sender: UIBarButtonItem)
    {
        performSegue(withIdentifier: "uno", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "noConex"
        {
            var vc = segue.destination as! UrgenciaSinConexionViewController
        }
        else
        {
            var vc = segue.destination as! MapaUrgenciaViewController
            vc.descript = myTextView.text
            
            vc.imagen = imageButton.currentImage
            vc.usuario = self.usuario
        }
        
    }
    
    @IBAction func isEditing(_ sender: UITextField)
    {
        textInput = sender.text!
        
        print(textInput)
    }
    
    // todo esto es para audio recorder //
    
    func getDocumentsDirector() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        return paths[0]
        
    }
    
    func setupRecorder() {
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .default)
        } catch let error as NSError {
            print(error.description)
        }
        
        let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0)),
                              AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC)),
                              AVNumberOfChannelsKey : NSNumber(value: 1),
                              AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.max.rawValue))]
        
        do{
            try soundRecorder = AVAudioRecorder(url: getDocumentsDirector().appendingPathComponent(fileName), settings: recordSettings)
            soundRecorder.delegate = self
            soundRecorder.prepareToRecord()
        }
        catch let error as NSError {
            error.description
        }
    }
    
    
    
    func setupPlayer() {
        
        let audioFilename = getDocumentsDirector().appendingPathComponent(fileName)
       
        do {
            
            soundPlayer = try AVAudioPlayer(contentsOf: audioFilename)
            
            soundPlayer.delegate = self
            
            soundPlayer.prepareToPlay()
            
            soundPlayer.volume = 100.0
            
        } catch {
            
            print(error)
            
        }
        
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        playButton.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        recordButton.isEnabled = true
        
        playButton.setTitle("Play", for: .normal)
        
    }
    
    
    
    // aqui termina audio recorder //
    
    
    
    @IBAction func selectImage(_ sender: UIButton)
    {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        
        
        let actionSheet = UIAlertController(title: "Seleccionar una fotografía", message: "Escoge de dónde quieres seleccionar la fotografía", preferredStyle: .actionSheet)
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                imagePickerController.sourceType = .camera
                
                self.present(imagePickerController, animated: true, completion: nil)
                
            }else{
                
                print("Camera is not available")
                
            }
            
            
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Galería", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imageButton.setImage(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, for: .normal)
       
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    // aqui termina lo del imagepicker
    
    
    
    // metodos action para el audiorecorder
    
    
    @IBAction func recordAction(_ sender: Any)
    {
        if recordButton.titleLabel?.text == "Record" {

            soundRecorder.record()
            lblValidationRecord.isHidden = false
            lblValidationRecord.text = "Presiona la opción de parar para dejar de grabar"
            recordButton.setTitle("Stop", for: .normal)
            recordButton.setImage(UIImage(named: "pause"), for: .normal)
            
            playButton.isEnabled = false
            playButton.setImage(UIImage(named: "camara"), for: .normal)
            
        } else {
            
            soundRecorder.stop()
            lblValidationRecord.isHidden = false
            lblValidationRecord.text = "Para reproducir debes pulsar la opción de play"
            recordButton.setTitle("Record", for: .normal)
            recordButton.setImage(UIImage(named: "microphone"), for: .normal)
            
            playButton.isEnabled = false
            playButton.setImage(UIImage(named: "camara"), for: .normal)
            
        }
    }
    
    
    @IBAction func playAction(_ sender: Any)
    {
        if playButton.titleLabel?.text == "Play" {
            
            lblValidationRecord.isHidden = false
            lblValidationRecord.text = "La nota de voz grabada se esta reproduciendo"
            
            playButton.setTitle("Stop", for: .normal)
            playButton.setImage(UIImage(named: "pause"), for: .normal)
            
            recordButton.isEnabled = false

            setupPlayer()
            
            soundPlayer.play()
            
            
            playButton.setImage(UIImage(named: "camara"), for: .normal)

            
        } else {
            
            soundPlayer.stop()
            lblValidationRecord.isHidden = false
            lblValidationRecord.text = "nota de voz grabada en pausa"
            
            playButton.setTitle("Play", for: .normal)
            playButton.setImage(UIImage(named: "camara"), for: .normal)
            recordButton.isEnabled = false
            
            
            
            playButton.setImage(UIImage(named: "camara"), for: .normal)
        }
    }
    
    
    
}
