//
//  ViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 3/18/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit
import FirebaseStorage

class MapaUrgenciaViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UISearchBarDelegate, GMSAutocompleteFetcherDelegate {
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
    }
    
    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }
    
    
    
    @IBOutlet weak var googleMapsViewContainer: UIView!
    let locationManager = CLLocationManager()
    let GoogleSearchPlaceApiKey = "AIzaSyDM-nmY6qNYj0Q67v0h743IPETNrTjRr50"
    
    
    var descript = ""
    var usuario = ""
    var lati: String!
    var longi: String!
    @IBOutlet weak var imageSelected: UIButton!
    var imagen: UIImage!
    var urlDescarga: URL!
    
    var imageReference: StorageReference{
        return Storage.storage().reference() .child("Resources")
    }
    
    
    var imageFfileName: String = "image.jpg"
    
    var cammara:GMSCameraPosition!
    var googleMapsView: GMSMapView!
    
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let x1 = UIImage(named: "com1")
        
        self.imageSelected.setImage(x1, for: .normal)
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let x = String((0..<6).map{ _ in letters.randomElement()! })
        imageFfileName = x+".jpg"
        
    }
    
    
    @IBAction func sendRequestUrgencia(_ sender: UIBarButtonItem)
    {
        self.performSegue(withIdentifier: "dos", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       
            self.imageSelected = UIButton()
       
       var termino = false
        
        guard let image = self.imagen else {return}
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {return}
        let uploadImageRef = self.imageReference.child(self.imageFfileName)
        let uploadTask = uploadImageRef.putData(imageData, metadata: nil) { (metadata, error) in
            
            termino = true
            let o = self.imageReference.child(self.imageFfileName)
            o.downloadURL(completion: { (url, error) in
                
                self.urlDescarga = url
                var vc = segue.destination as! ProcessingUrgenciaViewController
                
                vc.descriptionn = self.descript
                vc.lat = self.lati
                vc.long = self.longi
                vc.urlDownload = self.urlDescarga
                vc.usuario = self.usuario
            })
        }
        uploadTask.observe(.progress) { (snapshot) in
            print(snapshot.progress ?? "No more progress")
            
        }
        uploadTask.resume()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLocationOnMap()
        self.locationManager.stopUpdatingLocation()
    }
    
    
    func showCurrentLocationOnMap()
    {
        lati = "\(self.locationManager.location?.coordinate.latitude ?? 0.0)"
        longi = "\(self.locationManager.location?.coordinate.longitude ?? 0.0)"
        
        cammara = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 17.0)
        
        googleMapsView = GMSMapView.map(withFrame: self.googleMapsViewContainer.frame, camera: cammara)
        googleMapsView.delegate = self
        //view = mapView
        
        googleMapsView.settings.myLocationButton = true
        googleMapsView.isMyLocationEnabled = true
        
        let markker = GMSMarker()
        markker.position = cammara.target
        markker.snippet = "Current Location"
        markker.map = googleMapsView
        self.view.addSubview(self.googleMapsView)
        
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        let cammaraa = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 17.0)
        
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: cammaraa)
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        let markker = GMSMarker()
        markker.position = cammaraa.target
        markker.snippet = "Current Location"
        markker.map = mapView
        self.view.addSubview(mapView)
        view = mapView
        
    }
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String)
    {
        
        DispatchQueue.main.async { () -> Void in
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position: position)
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
            self.googleMapsView.camera = camera
            
            marker.title = "Address : \(title)"
            marker.map = self.googleMapsView
            
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        let placeClient = GMSPlacesClient()
        
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil)
        {
            (results, error: Error?) -> Void in
            
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            
            for result in results!
            {
                if let result = result as? GMSAutocompletePrediction
                {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
           
        }
        
        
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
        
        
    }
   
}
