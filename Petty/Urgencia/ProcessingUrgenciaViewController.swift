//
//  ProcessingUrgenciaViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 3/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class ProcessingUrgenciaViewController: UIViewController {

    var descriptionn = ""
    var lat = ""
    var long = ""
    var usuario = ""
    var urlDownload: URL!
    
    @IBOutlet weak var loadingIcon: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
            
            let urgenciasReference = Firestore.firestore().collection("urgencias")
            let parameters: [String: Any] = ["description":self.descriptionn, "latittude":self.lat, "longitude": self.long, "urlImage":self.urlDownload.absoluteString,"userId":self.usuario]
            urgenciasReference.addDocument(data: parameters)
            
            
            
            
            self.loadingIcon.stopAnimating()
            self.loadingIcon.hidesWhenStopped = true
            
            super.viewDidAppear(true)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let x = storyBoard.instantiateViewController(withIdentifier: "confirm") as! VeterinarioUrgenciaViewController
            self.present(x, animated: true, completion: nil)
            
            
        }
    }
}
