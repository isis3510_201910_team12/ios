//
//  FourthViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class FourthViewController: UIViewController {

    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var raza: UILabel!
    @IBOutlet weak var peso: UILabel!
    @IBOutlet weak var edad: UILabel!
    
    @IBOutlet weak var imagePet: UIImageView!
    
    var nombreText = ""
    var id = ""
    var urlDownload = ""
    var imageName = ""
    var raza2: String!
    var peso2: String!
    var edad2: String!
    var name: String!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.nombre.text = nombreText
        
        let mascotasReference = Firestore.firestore().collection("mascotas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            guard let snapshot = snapshot else { return }
            
            for document in snapshot.documents
            {
                let data = document.data()
                let space = "   "
                
                self.name = data["name"] as! String
                
                if self.name == self.nombreText
                {

                    self.nombre.text = space + self.name
                    self.raza2 = data["race"] as! String
                    self.raza.text = space + self.raza2
                    self.peso2 = data["weight"] as! String
                    self.peso.text = space + self.peso2
                    self.edad2 = data["age"] as! String
                    self.edad.text = space + self.edad2
                    self.id = data["id"] as! String
                    self.urlDownload = data["urlImage"] as! String
                    self.imageName = data["imageFileName"] as! String
                }
            }
        }
    }
    
    
    @IBAction func edit(_ sender: Any) {
        performSegue(withIdentifier: "edit", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! CreatePetViewController
        vc.image = self.imagePet.image
        vc.llegoImagen = "Si"
        vc.nameEdit = self.nombreText
        vc.raceEdit = self.raza2
        vc.weightEdit = self.peso2
        vc.ageEdit = self.edad2
        vc.id = self.id
        vc.urlDownload = self.urlDownload
        vc.imageName = self.imageName
    }
    
    
    
}
