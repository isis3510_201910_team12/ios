//
//  SecondViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

class SecondViewController: UIViewController {
    
    var titlesF = [String]()
    var vetF = [String]()
    var pricF = [String]()
    var imagesF = [UIImage]()
    var urlProductos = [String]()
    var nombresImagenes = [String]()
    
    var tipo: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.limpiarArreglos()
    }
    
    func limpiarArreglos()
    {
        self.titlesF.removeAll()
        self.vetF.removeAll()
        self.pricF.removeAll()
        self.imagesF.removeAll()
        self.urlProductos.removeAll()
        self.nombresImagenes.removeAll()
    }


    @IBAction func alimento(_ sender: Any)
    {
        self.tipo = "alimento"
        self.readProductos()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
    }
    
    @IBAction func medicamentos(_ sender: Any)
    {
        self.tipo = "medicamentos"
        self.readMedicamentos()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
        var vc = segue.destination as! CollectionViewController
        vc.tipo = self.tipo
        vc.titlesF = self.titlesF
        vc.vetF = self.vetF
        vc.pricF = self.pricF
        vc.imagesF = self.imagesF
        vc.urlProductos = self.urlProductos
        vc.nombresImagenes = self.nombresImagenes
        self.limpiarArreglos()
     }
    
    let alertService = AlertService()
    
    // todo lo de juguetes
    @IBOutlet weak var juguetesButton: UIButton!
    
    @IBAction func didTapJuguetes() {
       
        self.tipo = "juguetes"
        self.readJuguetes()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
    }
    
    // todo lo de casas
    
    
    @IBOutlet weak var casasButton: UIButton!
    
    @IBAction func didTapCasas() {
        
        self.tipo = "casas"
        self.readCasas()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            
            self.performSegue(withIdentifier: "productos", sender: self)
        }
    }
    
    
    // todo lo de collares
    
    @IBOutlet weak var collaresButton: UIButton!
    
    @IBAction func didTapCollares() {
       
        self.tipo = "collares"
        self.readCollares()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            
            self.performSegue(withIdentifier: "productos", sender: self)
        }
    }
    
    // todo lo de snacks
    
    @IBOutlet weak var snacksButton: UIButton!
    
    
    @IBAction func didTapSnacks() {
        
        self.tipo = "snacks"
        self.readSnacks()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
    }
    
    // todo lo de accesorios
    
    
    @IBOutlet weak var accesoriosButton: UIButton!
    
    @IBAction func didTapAccesorios() {
        
        self.readAccesorios()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
        
    }
    
    // todo lo de otros
    
    
    @IBAction func didTapOtros() {
       
        self.tipo = "otros"
        self.readOtros()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "productos", sender: self)
        }
        
    }
    
    
    @IBOutlet weak var otrosButton: UIButton!
    
    func readSnacks()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                if document.data()["tipo"] as!String == "snacks"
                {
                    
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readAccesorios()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "accesorios"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readOtros()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "otros"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readJuguetes()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "juguetes"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readCollares()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "collares"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readCasas()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "casas"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readMedicamentos()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                if document.data()["tipo"] as!String == "medicamento"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
        }
    }
    
    func readProductos()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("productos")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                if document.data()["tipo"] as!String == "alimento"
                {
                    let name = document.data()["name"] ?? ""
                    let precio = document.data()["precio"] ?? ""
                    
                    let url = document.data()["url"] ?? ""
                    let veterinaria = document.data()["veterinaria"] ?? ""
                    let ifn = document.data()["imageFileName"] ?? ""
                    self.nombresImagenes.append(ifn as! String)
                    self.urlProductos.append(data["url"] as! String)
                    
                    self.titlesF.append(name as! String)
                    
                    self.vetF.append(veterinaria as! String)
                    
                    self.pricF.append(precio as! String )
                    
                }
            }
            
        }
    }
  
}

