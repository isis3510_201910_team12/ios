//
//  ThirdViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase

import FirebaseFirestore

import FirebaseStorage

import Kingfisher


class ThirdViewController: UIViewController {
    var titlesF = [String]()
    
    var racesF = [String]()
    
    var agesF = [String]()
    
    var vetsF = [String]()
    
    var imagesF = [UIImage]()
    
    var urlEncontradasPerdidas = [String]()
    
    var urlAdoptadas = [String]()
    
    var nombresImagenes = [String]()
    
    var ids = [String]()
    var tipo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.limpiarArreglos()
        
        // Do any additional setup after loading the view.
    }
    
    func limpiarArreglos()
    {
        self.titlesF.removeAll()
        self.racesF.removeAll()
        self.agesF.removeAll()
        self.imagesF.removeAll()
        self.urlAdoptadas.removeAll()
        self.nombresImagenes.removeAll()
        self.vetsF.removeAll()
        self.urlEncontradasPerdidas.removeAll()
    }
    
    
    @IBAction func adoptarButton(_ sender: Any) {
        self.tipo = "adoptadas"
        self.read()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.performSegue(withIdentifier: "adoptar", sender: self)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.tipo = ""
    }
    
    @IBAction func listado(_ sender: Any)
    {
        let actionSheet = UIAlertController(title: "Selecciona", message: "¿Qué listado deseas ver?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Mascotas encontradas", style: .default, handler: { (action:UIAlertAction) in
            self.tipo = "encontradas"
            self.readEncontradas()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.performSegue(withIdentifier: "listam", sender: self)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Mascotas perdidas", style: .default, handler: { (action:UIAlertAction) in
            
            self.tipo = "perdidas"
            self.readPerdidas()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.performSegue(withIdentifier: "listam", sender: self)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        self.present(actionSheet, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if self.tipo == "encontradas"
        {
            var vc = segue.destination as! ListaRescateViewController
            vc.tipo = self.tipo
            vc.titlesF = self.titlesF
            vc.imagesF = self.imagesF
            vc.urlEncontradasPerdidas = self.urlEncontradasPerdidas
        }
        
        if self.tipo == "perdidas"
        {
            var vc = segue.destination as! ListaRescateViewController
            vc.tipo = self.tipo
            vc.titlesF = self.titlesF
            vc.imagesF = self.imagesF
            vc.urlEncontradasPerdidas = self.urlEncontradasPerdidas
        }
        if self.tipo == "adoptadas"
        {
            var vc = segue.destination as! AdoptarViewController
            vc.tipo = self.tipo
            vc.titlesF = self.titlesF
            vc.vetsF = self.vetsF
            vc.racesF = self.racesF
            vc.imagesF = self.imagesF
            vc.agesF = self.agesF
            vc.urlAdoptadas = self.urlAdoptadas
            vc.nombresImagenes = self.nombresImagenes
            self.limpiarArreglos()
        }
        
    }
    
    func readEncontradas()
        
    {
        self.limpiarArreglos()
        
        let mascotasReference = Firestore.firestore().collection("encontradas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            
            
            guard let snapshot = snapshot else { return }
            
            
            
            var c = 1;
            
            for document in snapshot.documents
                
            {
                
                let data = document.data()
                self.titlesF.append(data["descripcion"] as! String)
                
                
                
                let ifn = document.data()["imageFileName"] ?? ""
                
                self.urlEncontradasPerdidas.append(data["url"] as! String)
                
            }
            
        }
        
    }
    
    func readPerdidas()
        
    {
        self.limpiarArreglos()
        
        let mascotasReference = Firestore.firestore().collection("perdidas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            
            
            guard let snapshot = snapshot else { return }
            
            
            
            var c = 1;
            
            for document in snapshot.documents
                
            {
                
                let data = document.data()
                
                self.titlesF.append(data["descripcion"] as! String)
                
                let ifn = document.data()["imageFileName"] ?? ""
                
                self.urlEncontradasPerdidas.append(data["url"] as! String)
            }
            
        }
        
    }
    
    func read()
    {
        self.limpiarArreglos()
        let productsReference = Firestore.firestore().collection("adoptadas")
        productsReference.addSnapshotListener { (snapshot, _) in
            guard let snapshot = snapshot else { return }
            for document in snapshot.documents
            {
                let data = document.data()
                
                let name = document.data()["name"] ?? ""
                let edad = document.data()["age"] ?? ""
                let raza = document.data()["race"] ?? ""
                let veterinaria = document.data()["veterinaria"] ?? ""
                let ifn = document.data()["imageFileName"] ?? ""
                self.nombresImagenes.append(ifn as! String)
                self.urlAdoptadas.append(data["urlImage"] as! String)
                
                self.titlesF.append(name as! String)
                
                self.racesF.append(raza as! String)
                self.agesF.append(edad as! String )
                self.vetsF.append(veterinaria as! String)
                
            }
        }
    }
    
}
