//
//  ListaMascotasViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 3/22/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import Kingfisher

class ListaMascotasViewController: UIViewController {
    
  
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    
    @IBOutlet weak var lbl1: UILabel!
    
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var lbl4: UILabel!
    
    var prueba: UIImageView!
    var selectedName: String!
    var imageReference: StorageReference{
        return Storage.storage().reference() .child("Resources")
    }
    
    var id1 = ""
    var id2 = ""
    var id3 = ""
    var id4 = ""
    var usuario = ""
    
    let alertService = AlertService()
    let network: NetworkManager = NetworkManager.sharedInstance
    var quieroEliminar = "No"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
         self.leerUsuarios()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        self.leerUsuarios()
        
        
        
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
        
        // Do any additional setup after loading the view.
            self.quieroEliminar = "No"
            var lblActual = self.lbl1
            var imgActual = self.img1
        
        
        var urls = [String]()
        let mascotasReference = Firestore.firestore().collection("mascotas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            guard let snapshot = snapshot else { return }
            
            var c = 0;
            for document in snapshot.documents
            {
                let data = document.data()
               
                let imageName = data["imageFileName"]
                
                let downUrl = data["urlImage"] as! String
                print("")
                print("url")
                print(downUrl)
                
                
                let usuarioMascota = data["userId"] as! String
                
                var d = 0
                for x in urls
                {
                    if x == downUrl
                    {
                       d = d + 1
                    }
                    
                }
                print(d)
                print("")
                
                if d < 1 && self.usuario.trimmingCharacters(in: .whitespacesAndNewlines) == usuarioMascota.trimmingCharacters(in: .whitespacesAndNewlines)
                {
                    urls.append(downUrl as! String)
                }
                
                
                let name: String
                name = data["name"] as! String
                
                print("")
                print("Acaaa")
                print(self.usuario)
                print(usuarioMascota)
                print("")
                
                        if imgActual == self.img1
                        {
                            
                            if name != self.lbl2.text && name != self.lbl3.text && name != self.lbl4.text && self.usuario.trimmingCharacters(in: .whitespacesAndNewlines) == usuarioMascota.trimmingCharacters(in: .whitespacesAndNewlines)
                            {
                           
                            self.img1.kf.indicatorType = .activity
                                
                            self.img1.kf.setImage(with: URL(string: urls[0]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                            self.lbl1.text = name
                            self.id1 = document.documentID as! String
                            imgActual = self.img2
                                
                                
                            }
                            else
                            {
                                self.img1.kf.indicatorType = .activity
                                
                                self.img1.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl1.text = "Acá va tu primera mascota"
                                self.id1 = ""
                                
                                
                                self.img2.kf.indicatorType = .activity
                                
                                self.img2.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl2.text = "Acá va tu segunda mascota"
                                self.id2 = ""
                                
                                self.img3.kf.indicatorType = .activity
                                
                                self.img3.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl3.text = "Acá va tu tercera mascota"
                                self.id3 = ""
                                
                                
                                self.img4.kf.indicatorType = .activity
                                
                                self.img4.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl4.text = "Acá va tu cuarta mascota"
                                self.id4 = ""
                                //imgActual = self.img2
                            }
                        }
                        else if imgActual == self.img2
                        {
                            if name != self.lbl1.text && name != self.lbl3.text && name != self.lbl4.text && self.usuario.trimmingCharacters(in: .whitespacesAndNewlines) == usuarioMascota.trimmingCharacters(in: .whitespacesAndNewlines)
                            {
                            
                                self.img2.kf.indicatorType = .activity
                                
                                self.img2.kf.setImage(with: URL(string: urls[1]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                
                            self.lbl2.text = name
                            self.id2 = document.documentID as! String
                            imgActual = self.img3
                            }
                            else
                            {
                                self.img2.kf.indicatorType = .activity
                                
                                self.img2.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl2.text = "Acá va tu segunda mascota"
                                self.id2 = ""
                                
                                self.img3.kf.indicatorType = .activity
                                
                                self.img3.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl3.text = "Acá va tu tercera mascota"
                                self.id3 = ""
                                
                                
                                self.img4.kf.indicatorType = .activity
                                
                                self.img4.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl4.text = "Acá va tu cuarta mascota"
                                self.id4 = ""
                                
                                
                            }
                        }
                        else if imgActual == self.img3
                        {
                            
                            if name != self.lbl2.text && name != self.lbl1.text && name != self.lbl4.text && self.usuario.trimmingCharacters(in: .whitespacesAndNewlines) == usuarioMascota.trimmingCharacters(in: .whitespacesAndNewlines)
                            {
                                
                           
                                self.img3.kf.indicatorType = .activity
                                
                                print("")
                                print(name)
                                print(urls)
                                print("")
                                
                                self.img3.kf.setImage(with: URL(string: urls[2]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                            self.lbl3.text = name
                            self.id3 = document.documentID as! String
                            imgActual = self.img4
                            }
                            else
                            {
                                self.img3.kf.indicatorType = .activity
                                
                                self.img3.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl3.text = "Acá va tu tercera mascota"
                                self.id3 = ""
                                
                                self.img4.kf.indicatorType = .activity
                                
                                self.img4.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl4.text = "Acá va tu cuarta mascota"
                                self.id4 = ""
                               
                            }
                        }
                        else if imgActual == self.img4
                        {
                            if name != self.lbl2.text && name != self.lbl3.text && name != self.lbl1.text && self.usuario.trimmingCharacters(in: .whitespacesAndNewlines) == usuarioMascota.trimmingCharacters(in: .whitespacesAndNewlines) && urls.count > 2
                            {
                           
                                self.img4.kf.indicatorType = .activity
                                
                                self.img4.kf.setImage(with: URL(string: urls[3]), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                            self.lbl4.text = name
                            self.id4 = document.documentID as! String
                            imgActual = self.img1
                            }
                            else
                            {
                                self.img4.kf.indicatorType = .activity
                                
                                self.img4.kf.setImage(with: URL(string: "x"), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
                                self.lbl4.text = "Acá va tu cuarta mascota"
                                self.id4 = ""
                               
                            }
                        }
            }
            
            
        
        }
    }
    
    func leerUsuarios()
    {
        let mascotasReference = Firestore.firestore().collection("usuarios")
        
        
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            guard let snapshot = snapshot else { return }
            
            if !snapshot.documents.isEmpty
            {
                for document in snapshot.documents
                {
                    let num = document.data()["num"] as! String
                    if num == UserDefaults.standard.getNumber()
                    {
                        self.usuario = num
                    }
                    
                }
            }
        }
    }
    
    func showOfflinePage()
    {
        let alertVC = alertService.alert(title: "No hay conexion a internet ", body: "Es posible que no puedas ver las últimas actualizaciones sobre tus mascotas, para poder hacerlo conectate a internet. ", buttonTitle: "Entendido") {
            
        }
        
        present(alertVC, animated: true)
    }
    
    @IBAction func onClickBut1(_ sender: Any)
    {
        if self.quieroEliminar == "Si"
        {
            
            if self.img1.image != prueba
            {
                
                let actionSheet = UIAlertController(title: "Eliminar", message: "Esta mascota se eliminará de tu cuenta", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (action:UIAlertAction) in
                    
                    let mascotasReference = Firestore.firestore().collection("mascotas")
                    mascotasReference.addSnapshotListener { (snapshot,_) in
                        
                        if self.quieroEliminar == "Si"
                        {
                        guard let snapshot = snapshot else { return }
                        if self.id1 != ""
                        {
                            mascotasReference.document(self.id1).delete()
                            self.quieroEliminar = "No"
                        }
                        
                        }
                        
                        
                    }
                    
                    self.img1.image = nil
                    self.lbl1.text = "Acá va tu primera mascota"
                }))
                
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(actionSheet, animated: true, completion: nil)
                
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if self.img1.image != prueba
            {
                
                selectedName = self.lbl1.text
                performSegue(withIdentifier: "detailPet", sender: self)
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    

    @IBAction func onClickBut2(_ sender: Any)
    {
        if self.quieroEliminar == "Si"
        {
            if self.img2.image != prueba
            {
                let actionSheet = UIAlertController(title: "Eliminar", message: "Esta mascota se eliminará de tu cuenta", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (action:UIAlertAction) in
                    
                    let mascotasReference = Firestore.firestore().collection("mascotas")
                    mascotasReference.addSnapshotListener { (snapshot,_) in
                        if self.quieroEliminar == "Si"
                        {
                        guard let snapshot = snapshot else { return }
                        if self.id2 != ""
                        {
                            mascotasReference.document(self.id2).delete()
                            self.quieroEliminar = "No"
                        }
                        }
                    }
                    
                    self.img2.image = nil
                    self.lbl2.text = "Acá va tu segunda mascota"
                }))
                
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(actionSheet, animated: true, completion: nil)
                
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if self.img2.image != prueba
            {
                selectedName = self.lbl2.text
                performSegue(withIdentifier: "detailPet", sender: self)
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func onClickBut3(_ sender: Any)
    {
        if self.quieroEliminar == "Si"
        {
            if self.img3.image != prueba
            {
                let actionSheet = UIAlertController(title: "Eliminar", message: "Esta mascota se eliminará de tu cuenta", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (action:UIAlertAction) in
                    
                    let mascotasReference = Firestore.firestore().collection("mascotas")
                    mascotasReference.addSnapshotListener { (snapshot,_) in
                        if self.quieroEliminar == "Si"
                        {
                            guard let snapshot = snapshot else { return }
                            if self.id3 != ""
                            {
                                mascotasReference.document(self.id3).delete()
                                self.quieroEliminar = "No"
                            }
                        }
                    }
                    
                    self.img3.image = nil
                    self.lbl3.text = "Acá va tu tercera mascota"
                }))
                
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(actionSheet, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if self.img3.image != prueba
            {
                selectedName = self.lbl3.text
                performSegue(withIdentifier: "detailPet", sender: self)
                
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func onClickBut4(_ sender: Any)
    {
        if self.quieroEliminar == "Si"
        {
            if self.img4.image != prueba
            {
                let actionSheet = UIAlertController(title: "Eliminar", message: "Esta mascota se eliminará de tu cuenta", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "Eliminar", style: .destructive, handler: { (action:UIAlertAction) in
                    
                    let mascotasReference = Firestore.firestore().collection("mascotas")
                    mascotasReference.addSnapshotListener { (snapshot,_) in
                        if self.quieroEliminar == "Si"
                        {
                        guard let snapshot = snapshot else { return }
                        if self.id4 != ""
                        {
                            mascotasReference.document(self.id4).delete()
                            self.quieroEliminar = "No"
                        }
                        }
                        
                    }
                    
                    self.img4.image = nil
                    self.lbl4.text = "Acá va tu cuarta mascota"
                }))
                
                
                actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(actionSheet, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            if self.img4.image != prueba
            {
                selectedName = self.lbl4.text
                performSegue(withIdentifier: "detailPet", sender: self)
            }
            else
            {
                let alert = UIAlertController(title: "Ups", message: "No tienes ninguna mascota aquí.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "detailPet"
        {
            var vc = segue.destination as! FourthViewController
            vc.nombreText = self.selectedName
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1)
            {
                if self.selectedName == self.lbl1.text
                {
                    vc.imagePet.image = self.img1.image
                }
                else if self.selectedName == self.lbl2.text
                {
                    vc.imagePet.image = self.img2.image
                }
                else if self.selectedName == self.lbl3.text
                {
                    vc.imagePet.image = self.img3.image
                }
                else if self.selectedName == self.lbl4.text
                {
                    vc.imagePet.image = self.img4.image
                }
                
            }
        }
        else if segue.identifier == "add"
        {
            var vc = segue.destination as! CreatePetViewController
            vc.llegoImagen = "No"
        }
        
    }
    
    @IBAction func add(_ sender: Any) {
        performSegue(withIdentifier: "add", sender: self)
        
    }
    
    
    @IBAction func remove(_ sender: Any) {
        
        self.quieroEliminar = "Si"
        
        let alert = UIAlertController(title: "Eliminar", message: "Selecciona la mascota que deseas eliminar.", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(subButton)
        self.present(alert, animated: true, completion: nil)
    }

}
