//
//  FirstViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 2/21/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class FirstViewController: UIViewController {
    
    let alertService = AlertService()
    
    let network: NetworkManager = NetworkManager.sharedInstance
    let network2: NetworkManagerUrgencias = NetworkManagerUrgencias.sharedInstance
    
    var tipoServicio: String!
    var usuario = ""
    var x = 0
    
    
    // maneja la alerta de en progreso

    @IBOutlet weak var alertButton: UIButton!

   
    @IBAction func logoutUser(_ sender: Any) {
        
        self.x = 1
        let mascotasReference = Firestore.firestore().collection("usuarios")
        
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
           
            guard let snapshot = snapshot else { return }

                for document in snapshot.documents
                {
                    if self.x > 0
                    
                    {
                        
                        mascotasReference.document(document.documentID).delete()
                    }
                }
    
        }
        
        self.seleccionarUsuario()
        
    }
    
        
    
    @IBAction func servicioVacunacion(_ sender: Any)
    {
        self.tipoServicio = "vacunacion"
        self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
    }
    
    // servico baño
    
    @IBOutlet weak var servicioBañoButton: UIButton!
    @IBAction func didTapServicioBaño() {
        
        self.tipoServicio = "baño"
        self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
       
    }
    // servicio paseador
    
    @IBOutlet weak var servicioPaseadorButton: UIButton!
    @IBAction func didTapServicioPaseador() {
        self.tipoServicio = "paseador"
        self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
    }
    
    // servicio funeraria
    
    
    @IBOutlet weak var servicioFunerariaButton: UIButton!
    @IBAction func didTapServicioFuneraria() {
        
        self.tipoServicio = "funeraria"
         self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
    }
    
    // partos y esterilizacion
    
    @IBOutlet weak var servicioPartosButton: UIButton!
    
    @IBAction func didTapServicioPartos() {
        
        self.tipoServicio = "partos"
        self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
        
    }
    // desparasitacion
    
    
    @IBOutlet weak var servicioDesparasitacionButton: UIButton!
    
    @IBAction func didTapServicioDesparasitacion() {
        self.tipoServicio = "desparasitacion"
        self.performSegue(withIdentifier: "serviciosAbajo", sender: self)
        
    }
    
    // aqui termina todo lo del manejo de la alerta
    
    override func viewDidAppear(_ animated: Bool) {
        NetworkManagerUrgencias.stopNotifier()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        

        let mascotasReference = Firestore.firestore().collection("usuarios")
        
        
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        
        let mascotasReference = Firestore.firestore().collection("mascotas")
        
        mascotasReference.addSnapshotListener { (snapshot,_) in
            
            guard let snapshot = snapshot else { return }
            
                if snapshot.documents.isEmpty
                {
                let alert = UIAlertController(title: "Bienvenido", message: "Aún no tienes ninguna mascota registrada. Por favor agrega tus mascotas para poder acceder a todos nuestros servicios.", preferredStyle: .alert)
                let subButton = UIAlertAction(title: "Ok", style: .default, handler: self.registrarMascota)
                
                alert.addAction(subButton)
                self.present(alert, animated: true, completion: nil)
                }
            }
           
        }
    }
       
        func showOfflinePage() {
            let alertVC = alertService.alert(title: "No hay conexion a internet ", body: "Revisa tu conexión de internet para tener acceso a todos nuestros servicios ", buttonTitle: "Entendido") {
                
            }
            
            present(alertVC, animated: true)
        }
    
        func registrarMascota(alert: UIAlertAction)
        {
          
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let x = storyBoard.instantiateViewController(withIdentifier: "create") as! CreatePetViewController
            self.navigationController?.pushViewController(x, animated: true)
        }
    
    
    func seleccionarUsuario()
    {
        
        self.x = 0
        self.tipoServicio = "logout"
        self.performSegue(withIdentifier: "logout", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if self.tipoServicio == "logout"
        {
             var vc = segue.destination as! StartViewController
            vc.tipo = "logout"
        }
        else if self.tipoServicio == "urgencia"
        {
            var vc = segue.destination as! UrgenciaViewController
            vc.usuario = self.usuario
            
        }
        else
        {
            var vc = segue.destination as! MapaServiciosViewController
            vc.tipoService = self.tipoServicio
            vc.usuario = self.usuario
        }
        
    }
    
    
    @IBAction func urgencia(_ sender: Any)
    {
        self.tipoServicio = "urgencia"
        self.performSegue(withIdentifier: "urgencia", sender: self)
    }
    
}

