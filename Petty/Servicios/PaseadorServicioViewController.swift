//
//  PaseadorServicioViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class PaseadorServicioViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    var tipoServ: String!
    var fecha: String!
    var lat = ""
    var long = ""
    var usuario = ""
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func slide(_ sender: UISlider)
    {
        label.text = String(Int(sender.value)) + " Horas"
    }
    
    @IBAction func solicitar(_ sender: Any)
    {
        self.performSegue(withIdentifier: "enviarDatos2", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! ProcessingServiciosViewController
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yy, H:mm"
        vc.fecha = formater.string(from:datePicker.date)
        vc.franja = label.text as! String
        vc.tipoServicio = self.tipoServ
        vc.lat = self.lat
        vc.long = self.long
        vc.usuario = self.usuario
    }
}
