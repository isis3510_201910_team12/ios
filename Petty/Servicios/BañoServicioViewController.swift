//
//  BañoServicioViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class Ban_oServicioViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    var tipoServ: String!
    var fecha: String!
    var lat = ""
    var long = ""
    var usuario = ""
    
    @IBOutlet weak var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.text = "Tipo de servicio:"+self.tipoServ
        
    }
    
    @IBAction func solicitar(_ sender: Any)
    {
        self.performSegue(withIdentifier: "enviarDatos1", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! ProcessingServiciosViewController
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yy, H:mm"
        vc.fecha = formater.string(from:datePicker.date)
       
        vc.tipoServicio = self.tipoServ
        vc.lat = self.lat
        vc.long = self.long
        vc.usuario = self.usuario
    }
}
