//
//  ProcessingServiciosViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase

class ProcessingServiciosViewController: UIViewController {
    
    var fecha = ""
    var vacuna = ""
    var tipoServicio = ""
    var lat = ""
    var long = ""
    var franja = ""
    var tipoFuneraria = ""
    var usuario = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        let serviciosReference = Firestore.firestore().collection("servicios")
        let parameters: [String: Any] = ["fecha":self.fecha, "latitud":self.lat, "longitud": self.long, "vacuna":self.vacuna,"tipoServicio":self.tipoServicio, "franja":self.franja, "tipoFuneraria": self.tipoFuneraria,"userId":self.usuario]
        
        serviciosReference.addDocument(data: parameters)
        // Do any additional setup after loading the view.
    
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let alert = UIAlertController(title: "Confirmación", message: "Tu pedido ha sido registrado exitosamente.", preferredStyle: .alert)
        let subButton = UIAlertAction(title: "Ok", style: .default, handler: self.regresar)
        
        alert.addAction(subButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func regresar(alert: UIAlertAction)
    {
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let x = storyBoard.instantiateViewController(withIdentifier: "y") as! CustomTabBarViewController
        x.usuario = self.usuario
        self.present(x, animated: true, completion: nil)
        
    }

}
