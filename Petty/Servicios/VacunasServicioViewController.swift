//
//  VacunasServicioViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class VacunasServicioViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
 
    

    let dataSource = ["Varicela","Hepatitis A", "Hepatitis B", "VPH", "Triple viral", "Antimeningocócica", "Polio", "rotavirus", "Culebrilla"]
    @IBOutlet weak var vacunasPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var fecha: String!
    var vacuna = "Varicela"
    var lat = ""
    var long = ""
    var usuario = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vacunasPicker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        vacuna = dataSource[row]
    }
    
    
    @IBAction func continuar(_ sender: Any)
    {
        self.performSegue(withIdentifier: "enviarDatos", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! ProcessingServiciosViewController
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yy, H:mm"
        vc.fecha = formater.string(from:datePicker.date)
        vc.vacuna = self.vacuna
        vc.tipoServicio = "vacunacion"
        vc.lat = self.lat
        vc.long = self.long
        vc.usuario = self.usuario
    }

}
