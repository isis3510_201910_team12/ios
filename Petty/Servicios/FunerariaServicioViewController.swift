//
//  FunerariaServicioViewController.swift
//  Petty
//
//  Created by CAMILO ANDRES ANZOLA GONZALEZ on 4/24/19.
//  Copyright © 2019 CAMILO ANDRES ANZOLA GONZALEZ. All rights reserved.
//

import UIKit

class FunerariaServicioViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource {
    
    let dataSource = ["Cremación", "Cementerio"]
    var tipo = "Cremación"
    var explicacionCremacion = "Traslado del cuerpo, Cremación individual CON devolución de cenizas, Planta recordatorio, Guía de Duelo y diploma personalizado, Charla duelo, Cementerio Virtual, Asistencia al horno crematorio"
    var explicacionCementerio = "Traslado cuerpo, Lote en tierra por 5 años, ataúd ecológico para menores de 19 Kgs, lápida tallada en Mármol, Guía de Duelo y diploma personalizado, Baúl recordatorio, Charla duelo"
    
    @IBOutlet weak var tipoPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var tipoServ: String!
    var fecha: String!
    var lat = ""
    var long = ""
    var usuario = ""
    
    @IBOutlet weak var c: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tipoPicker.delegate = self
        
        c.text = self.explicacionCremacion
        self.c.layer.cornerRadius = 8
        self.c.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1).cgColor
        self.c.layer.borderWidth = 1.0
        // Do any additional setup after loading the view.
    }
    

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tipo = dataSource[row]
        
        if tipo == "Cremación"
        {
            c.text = self.explicacionCremacion
        }
        else
        {
            c.text = self.explicacionCementerio
        }
    }
    
    @IBAction func solicitar(_ sender: Any)
    {
        self.performSegue(withIdentifier: "enviarDatos3", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        var vc = segue.destination as! ProcessingServiciosViewController
        let formater = DateFormatter()
        formater.dateFormat = "dd/MM/yy, H:mm"
        vc.fecha = formater.string(from:datePicker.date)
        vc.tipoFuneraria = self.tipo
        vc.tipoServicio = self.tipoServ
        vc.lat = self.lat
        vc.long = self.long
        vc.usuario = self.usuario
    }
}
